# coding: utf-8

require './trace.rb'

module Proba

  def Proba.fact (n)
    n <= 1 ? 1 : (n * fact(n - 1))
  end

  def Proba.cnp (n, p)
    res = 1
    n.times { |i| res *= (p - i) }
    return (res / fact(n))
  end

  def Proba.loiBinomiale (avg)
    beg = Time.now
    prob = avg / (8.0 * 3600.0)
    res = cnp(25, 3500) * (prob**25) * ((1 - prob)**(3500 - 25))
    res *= 100
    time = (Time.now - beg) * 1000
    puts "Loi binomiale:"
    puts "\t\tTemps de calcul: #{time.round(3)} ms"
    puts "\t\tProbabilité d'encombrement: #{res.round(1)}%"
  end

  def Proba.loiPoisson (avg)
    beg = Time.now 
    l = 3500 * avg / (8.0 * 3600.0)
    res = Math.exp(-l) * (l**25) / fact(25)
    res *= 100
    time = (Time.now - beg) * 1000
    puts "Loi de Poisson:"
    puts "\t\tTemps de calcul: #{time.round(3)} ms"
    puts "\t\tProbabilité d'encomrement: #{res.round(1)}%"
  end

  def Proba.loiBinomialeGraph (avg, k)    
    prob = avg / (8.0 * 3600.0)
    res = cnp(k, 3500) * (prob**k) * ((1 - prob)**(3500 - k))
    #res *= 100
  end

  def Proba.loiPoissonGraph (avg, k)
    l = 3500 * avg / (8.0 * 3600.0)
    res = Math.exp(-l) * (l**k) / fact(k)
    #res *= 100
  end
end
