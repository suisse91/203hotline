# coding: utf-8

require 'gruff'

class BarGraph
  def initialize
    @binomial = Array.new
    @poisson = Array.new
    @graph = Gruff::Bar.new
    @graph.y_axis_increment = 0.02
    @graph.maximum_value = 0.12
    @graph.minimum_value = 0.0
    @graph.title = "203hotline"
    @graph.x_axis_label = "Nombre d'appels simultanés"
    @graph.y_axis_label = "Probabilité"
    6.times { |i| @graph.labels[i*10] = (i * 10).to_s }
  end

  def addValBinomial (val)
    @binomial << val
  end

  def addValPoisson (val)
    @poisson << val
  end

  def render
    @graph.data('loi Binomiale', @binomial)
    @graph.data('loi de Poisson', @poisson)
    @graph.write("graph.png")
  end
end
